var utils = require("../../../utils/utils");
Page({
    data: {
        logo: ['../../../images/logo1.png', '../../../images/logo2.png'],
        imgs: [
            "../../../images/blog2.jpg",
            "../../../images/blog3.jpg",
            "../../../images/blog.jpg",
        ],
        showDetail: true,
        section: [
            `
            &emsp;&emsp;博客最初的名称是Weblog，由web和log两个单词组成，字面意思是网络日记，后来喜欢新名词的人把这个词的发音故意改了一下，读成we blog，由此，blog这个词被创造出来。

            &emsp;&emsp;中文意思即网志或网络日志，不过，在中国大陆有人往往也将Blog本身和blogger（即博客作者）均音译为“博客”。“博客”有较深的涵义：“博”为“广博”；“客”不单是“blogger”更有“好客”之意。
            `,
            `
            &emsp;&emsp;1、亲朋之间的博客（家庭博客）：这种类型博客的成员主要由亲属或朋友构成，他们是一种生活圈、一个家庭或一群项目小组的成员。

            &emsp;&emsp;2、协作式的博客：，其主要目的是通过共同讨论使得参与者在某些方法或问题上达成一致，通常把协作式的博客定义为允许任何人参与、发表言论、讨论问题的博客日志。

            &emsp;&emsp;3、公共社区博客：公共出版在几年以前曾经流行过一段时间，但是因为没有持久有效的商业模型而销声匿迹了。廉价的博客与这种公共出版系统有着同样的目标，但是使用更方便，所花的代价更小，所以也更容易生存。
            `,
            `
            
            个人博客具有的优点： 
            &emsp;&emsp;&emsp;* 个人自由表达和出版；
            &emsp;&emsp;&emsp;* 知识过滤与积累；
            &emsp;&emsp;&emsp;* 深度交流沟通的网络新方式。
            &emsp;&emsp;&emsp;* 博客永远是共享与分享精神的体现。

            &emsp;&emsp;&emsp;1、作为网络个人日记
            &emsp;&emsp;&emsp;2、个人展示自己某个方面的空间
            &emsp;&emsp;&emsp;3、网络交友的地方
            &emsp;&emsp;&emsp;4、学习交流的地方
            &emsp;&emsp;&emsp;5、通过博客展示自己的企业形象或企业商务活动信息

            &emsp;&emsp;想了解更多个人博客的信息请关注:
            `
        ]
    },
    showDetail: true,
    // function

    toggleDetail: utils.toggleDetail,
    makePhone: utils.makePhone,
});