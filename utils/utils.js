//在setData函数中他的this指向Page函数中的这个setData应该是个隐式函数或者在其原型链上
function toggleDetail(e) {
    this.showDetail = !this.showDetail;
    this.setData({
        showDetail: this.showDetail 
    });
}
// 点击拨打电话
function makePhone(e){
    var id = e.currentTarget.id;
    if(id=='gyq') var number = '18342133452';
    else var number = '13130601143';
    wx.makePhoneCall({
        phoneNumber:number
    });
}

module.exports = {
    toggleDetail,
    makePhone,
}