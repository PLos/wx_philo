Page({
    data:{
        content:{
            contentTitle:'如何利用互联网打造自己的平台',
            content:`&nbsp;&nbsp;互联网像洪水猛兽给传统零售业带来了巨大的冲击和影响，传统的销售方式在互联网思维面前溃不成军，受到互联网的影响，大量消费者转移到线上购物，传统的门店客流量大幅缩减。他们没有雄厚的资本去大市场角逐，但互联网的冲击使他们的生意也日渐衰退，房租和管理成本的日益高涨，他们如今面临着生死存亡的问题。
            那么地方性的连锁零售如何与互联网的结合，打好自己的领地保卫战呢？
            连锁门店在经营中面临的问题和症结在哪儿？
            &nbsp;&nbsp;互联网将行业信息透明，电商抢夺了消费者
            1.现在都在讲互联网对零售行业带来了巨大的冲击，那冲击在哪？互联网的本质是连接和共享，它建立了用户与信息流之间的点对面的连接，让用户能够在互联网上看到所有同类产品的商家，同样也能了解到产品生产过程，原材料成本等信息，让行业信息不再神秘。商家赤身裸体的站在消费者面前等待消费者的临幸。移动互联网的发达，让消费者的购物更加便利，且更不可节制，除紧需物品外，消费者已很少走进门店，周六日的超级市场，也是人群逐渐减少。电子商务的成交额已占到商品经济的10%，并呈现几何式的增长。
            2传统门店成本高涨，管理效率低下
                日益高涨的房租费用，店面装修维护费用，店面面临各种管理收费居高不下，每天醒来已经欠下了高额的账单。店面的选址，装修都已不敢轻举妄动，相对于电商的商家门店费用高出多倍，成为拖垮门店经营者的最大负担，使得价格促销成为门店营销的禁区。管理总部与门店之间的信息不通畅，总部经营者失去了与消费者面对面交流的机会，使得信息交换缓慢，某个促销方案从制定到全部门店甚至需要两个月的时间，门店的效率低下使得经营者只能驻足不前。
            3生产效能低下，产品更新缓慢
                经营者失去了自己本身与消费者密切沟通的优势，产品的周转率降低，单品销量降低，使得原材料与供应链端成本增高，生产效能低下。部分自有工厂已经沦落为网商的代工厂，自己的产品排产和产能受到了限制。
                传统门店正面临着他们发展中的巨大困境，如何摆脱困境，找到新的爆发点，成为门店经营者的最大考验。
            互联网现在之于连锁门店已是重要的战略方向，而不是仅是小孩子的把戏。现在再不做互联网，将来真的无商可务。城区的连锁门店不要追求大而广，把产品卖出中国，卖向全世界，只需要将自己区域的用户服务好，最大化的发掘消费者的潜力价值，就能盆满鉢深。
            我们常讲互联网和电商是一把手工程，必须亲自抓，企业的互联网发展就是当前的重要战略任务，必须把互联网放到全局的高度去贯彻执行。做互联网需要紧抓三个点，一个是互联网推广传播，一个是互联网销售还有就是产品互联网化。
            企业互联网化要精心计划，要实现的布局是“互联网品牌传播+电商销售”两条腿走路，形成人未到，声先到的状态（类似于小米的饥饿营销）；运用大数据的分析，指导店内产品的研发与开店的位置选择；同时与电商平台合作拓展销售范围，将原来的3公里生活圈扩展到5公里甚至整个城区；积极的电商网站合作，发展网络经销商或是散单团购，提高单品销量，反向影响供应链，降低原材料价格；积极的进行口碑传播，让用户在网络上口口相传，提高门店的到店和消费人数，形成O2O的闭环。
            移动互联网的发展，网络信息的完善，用户随时都能接收到新信息，手机已成为人体的第二器官。信息的获取已从传统的纸媒，电视媒体，户外广告更依赖
            于移动互联网，更多的人愿意用微信和朋友交流，而不愿与其见面聊天。如今的我们更喜欢使用大众点评寻找附件的美食，使用美团寻找促销的商家。
             互联网的媒体的快速变革给了商家更多的机会，但是也带来了巨大的挑战，大众化的网络媒体平台已向用户开放，用户可以自己发文章，甚至自己能够成为媒体，获得粉丝。微博，微信公众号成为了当下互联网传播的最优工具，在微信，微博上获得粉丝关注的企业可以将自己的账号称之为自媒体。
              “电商直营旗舰店+联营平台+网络经销商”不失为现在商家做电商的最好布局。旗舰店作用为做产品展示，品牌信息传达，用户体验。而联营平台和网络经销商在旗舰店的光环下则主要进行产品销售。我们不建议建立企业的独立网站商城，推广成本高，难度大，品牌认知建立困难。经营者可以根据自己的情况选择销售店铺类型。可以在天猫，京东，饿了么，微信商城等建立自己的销售平台。
              销售的多种经营带动的是产品动销快，集中对某几款产品进行爆发式的销售，提高生产工厂在单品供应链生产效能，降低原材料及损耗。同时聚焦式的销售也更容易获得成功。`
        }
    }
    
});