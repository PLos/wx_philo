var utils = require('../../utils/utils');
Page({
  data: {
    logo: ['../../images/logo1.png', '../../images/logo2.png'],
    markers: [{
      id: 1,
      latitude: 40.885990,
      longitude: 121.062420,
      name: '菲络工作室',
      iconPath: '../../icons/coord.png',
      width: "80rpx",
      height: "80rpx"
    }],
    content: ["../../images/group0.jpg", "../../images/group1.jpg", "../../images/group2.jpg"],
    tell_us: [
      "&emsp;&emsp;菲络工作室是由一群90后青春、有创意、懂技术的年轻人组成的新兴组织。经过高校计算机技术系统性地学习。结合搭建学院官网,企业网站和开发个人博客,图书管理系统和参加省级App设计大赛等经历我们有充足的实战经验,目前已经为近百位用户提供过开发服务。我们竭尽全力为每一位客户提供优质服务。",
      `&emsp;&emsp;菲络工作室服务于中小型企业和个人,专注于网站、微信、安卓、后台管理系统的设计和开发。科班出身的我们对于软件性能的追求更高。对于网络、系统、数据结构、软件内部结构拼接耦合设计有完整系统认知和实践。较社会上的经过自学或培训机构出身的程序员和某些软件外包企业批量招收的人员培训一两月就上手为客户制作软件。相对来说我们更加专业，我们坚信理论指导实践。因为专业，所以安全。`
    ]
  },
  makePhone:utils.makePhone
})