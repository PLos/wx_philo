var utils = require("../../../utils/utils");
Page({
    data: {
        logo: ['../../../images/logo1.png', '../../../images/logo2.png'],
        imgs: [
            "../../../images/manage3.jpg",
            "../../../images/manage2.jpg",
            "../../../images/manage1.jpg",
        ],
        showDetail: true,
        section: [
            `
            &emsp;&emsp;您是否还在为工单重复，导致货物重复送出，出现客户“买一赠一”的状况而烦恼。您是否还在为员工职责、工资等改动的管理而烦恼。这就是公司发展到一定规模的通病————管理效率跟不上运营规模的发展。

            &emsp;&emsp;我们有着丰富的开发经验，类似于图书管理系统，货物管理系统，公司部门员工管理系统，客户管理系统，我们均可以为你量身定做合适的管理系统为您公司的发展保驾护航。
            `,
            `
            &emsp;&emsp;系统将协助您方便做好每一个记录的细节，还可以大大简化您的操作的步骤，帮助您从繁琐的管理事务中脱身，许多重复的步骤都可以帮你自动化完成。您的每一次的操作都将有记录日志，防止您的误操作。
            `,
            `
          较传统的桌面级管理系统性能上有更好的兼容性，移动端和电脑端可以做到较好的兼容，便于您的移动管理，依赖于网络进行内容的一致性整合。

          具体功能:

            &emsp;货物信息记录，查看，查重，分类，增删改。
            
            &emsp;员工信息记录，查看，管理。

            &emsp;销售信息记录，查看，管理，更改。

            &emsp;自动运算，排序，时间分配。

            &emsp;后台图像化显示，便于您对信息的分析，Excel表格的导出。

            &emsp;事务定时，可以自动化完成琐碎工作。

            &emsp;事务回滚，操作步骤的记录，防止误操作。

            &emsp;&emsp;想了解更多管理系统开发的信息请关注:
            `
        ]
    },
    showDetail: true,
    // function
    toggleDetail:utils.toggleDetail,
    makePhone:utils.makePhone,
});