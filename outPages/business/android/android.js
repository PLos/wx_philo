var utils = require("../../../utils/utils");

Page({
    data: {
        logo: ['../../../images/logo1.png', '../../../images/logo2.png'],
        imgs: [
            "../../../images/android.jpg",
            "../../../images/android2.jpg",
            "../../../images/cloud3.jpg"
        ],
        showDetail: true,
        section: [
            `
            &emsp;&emsp;App(Application)即手机应用的意思,如今使用移动电子产品的人均时长要远大于电脑端了。开发也因此转变成了以移动端优先。

            &emsp;&emsp;手机应用可以完成功能非常丰富，定位导航，在线直播，视频音乐，游戏，电话语音，社交软件，邮箱，大量的娱乐软件等等。
            `,
            `
            &emsp;&emsp;打开手机应用市场后映入眼帘的是各式各样的应用程序，种类繁多，数不胜数。从文本阅读，WIFI连接，到吃鸡游戏。功能由简入繁，我们作为安卓开发者，工作室内的Android开发组将全力为您服务，根据您的需求，为您进行个性化设计。
            `,
            `

          具体功能:

            &emsp;地图导航、定位、视频、音乐、上传、下载、脸部登陆、连接QQ\微信,电商等
            
            &emsp;微信和支付宝支付、在线聊天、页内搜索、页内留言、新闻发布、调用外部视频、邮件和电话验证等等

            &emsp;创意设计，根据用户提供的资源进行独具匠心的设计，注重用户体验

            &emsp;&emsp;想了解更多手机软件开发的信息请关注:
            `
        ]
    },
    showDetail: true,
    // function
    toggleDetail:utils.toggleDetail,
    makePhone:utils.makePhone
});