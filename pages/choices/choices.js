var utils = require("../../utils/utils");
Page({
    data: {
        logo: ['../../images/logo1.png', '../../images/logo2.png'],
        business: [{
                id: 'item0',
                url: "../../images/weixin1.jpg",
                busi: "微信小程序"
            },
            {
                id: 'item1',
                url: "../../images/weixin7.jpg",
                busi: "微信公众号"
            },
            {
                id: 'item2',
                url: "../../images/blog.jpg",
                busi: "个人博客"
            },
            {
                id: 'item3',
                url: "../../images/cloud3.jpg",
                busi: "手机应用"
            },
            {
                id: 'item4',
                url: "../../images/manage6.jpg",
                busi: "桌面版管理系统"
            },
            {
                id: 'item5',
                url: "../../images/manage1.jpg",
                busi: "网页版管理系统"
            },
            {
                id: 'item6',
                url: "../../images/ad0.jpg",
                busi: "网站开发"
            },
            {
                id: 'item7',
                url: "../../images/transaction.jpg",
                busi: "交易流程"
            },

        ],
        content: [
            "../../images/Paper4.jpg",
            "../../images/Paper3.jpg",
            "../../images/Paper1.jpg",
            "../../images/Paper2.jpg",
            "../../images/Paper5.jpg"
        ],
    },
    showPage(e) {
        var item = e.currentTarget.id; //获取bus-firstline-itemx
        var url = '../../outPages/business/';
        switch (item) {
            case 'item0':
                url += 'weixin/weixin';
                break;
            case 'item1':
                url += 'weixin2/weixin2';
                break;
            case 'item2':
                url += 'blog/blog';
                break;
            case 'item3':
                url += 'android/android';
                break;
            case 'item4':
                url += 'manage/manage';
                break;
            case 'item5':
                url += 'manage2/manage2';
                break;
            case 'item6':
                url += 'page/page';
                break;
            case 'item7':
                url += 'transaction/transaction';
                break;
        }
        wx.navigateTo({
            url
        });
    },

    makePhone:utils.makePhone,
})