var utils = require('../../utils/utils');
Page({
  data: {
    
    logo: ['../../images/logo1.png', '../../images/logo2.png'],
    content: [
      "../../images/Paper4.jpg",
      "../../images/Paper3.jpg",
      "../../images/Paper1.jpg",
      "../../images/Paper2.jpg",
      "../../images/Paper5.jpg"
    ],
    markers: [{
      id: 1,
      latitude: 40.88599,
      longitude: 121.06242,
      name: "菲络工作室",
      iconPath: "../../icons/choose2.png",
      width: "80rpx",
      height: "80rpx"
    }],
    content0: "风雨同行·共同成长。现在，服务的上千家中小型企业或组织与我们一路同行，见证我们的成长；未来，我们一起分享成功的喜悦。",
    content1: '服务之心·海纳百川。以客户需求为基础，以自身理念方向，成就客户，即成就我们!',
    news: [{
        news_id: 'news1',
        news_title: '对标小程序！安卓推出 “快应用” ！',
        news_content: '小米、华为、OPPO、VIVO等十家中国主流手机厂商聚集一起，要做一个针对微信小程序的快应用',
        time: {
          time: '2018/09/10'
        }
      },
      {
        news_id: 'news2',
        news_title: '微信小程序能给用户带来什么？',
        news_content: '微信小程序使用的用户越来越多，功能也越来越齐全，现在已经许多完美熟知的功能，如导航、直播、',
        time: {
          time: '2018/10/05'
        }
      },
      {
        news_id: 'news3',
        news_title: '如何合理利用网络塑造自己的平台',
        news_content: '现今是一个网络发达的时代，每个人都感同身受，很少有置身事外者，如何投身互联网来营造自己的平台',
        time: {
          time: '2018/10/28'
        }
      }
    ],
    // 业务模板数据
    business: [{
        mark1:false,
        mark2:false,
        whichLine: 'bus-firstline',
        whichItem: ['bus-firstline-item1', 'bus-firstline-item2', 'bus-firstline-item3'],
        src_img: ['../../icons/manage.png', '../../icons/weixin.png', '../../icons/web.png'],
        services: ['管理系统', '微信程序', '网站开发']
      },
      {
        mark1:false,
        mark2:false,
        whichLine: 'bus-secondline',
        whichItem: ['bus-secondline-item1', 'bus-secondline-item2', 'bus-secondline-item3'],
        src_img: ['../../icons/blog.png', '../../icons/android.png', '../../icons/aftersale.png'],
        services: ['个人博客', '安卓App', '流程步骤']
      }
    ]
  },


  /**
   * 页面切换方法
   */
  pageChange(e) {
    var id = e.currentTarget.id; //用于主页面新闻的跳转
    var tabChange = e.currentTarget.dataset.tabChange; //用于tabBar的跳转
    if ('news' == id.substring(0, id.length - 1)) {
      wx.navigateTo({
        url: '../../outPages/news/' + id + '/' + id
      });
    } else {
      wx.switchTab({
        url: "../" + tabChange + '/' + tabChange
      });
    }
  },
  /**
   * 展示业务页面
   */
  showPage(e) {
    var id = e.currentTarget.id; //获取bus-firstline-itemx
    var item = id.substr(id.indexOf('-') + 1);
    var url = '../../outPages/business/';
    switch (item) {
      case 'firstline-item1':
        url += 'manage/manage';
        break;
      case 'firstline-item2':
        url += 'weixin/weixin';
        break;
      case 'firstline-item3':
        url += 'page/page';
        break;
      case 'secondline-item1':
        url += 'blog/blog';
        break;
      case 'secondline-item2':
        url += 'android/android';
        break;
      case 'secondline-item3':
        url += 'transaction/transaction';
        break;
    }
    if ('firstline-item1' == item) {

    } else if ('firstline-item2' == item) {

    } else {
      wx.navigateTo({
        url
      });
    }

  },
  /**
   * 拨打电话
   */
  makePhone: utils.makePhone,
});