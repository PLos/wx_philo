var utils = require("../../../utils/utils");
Page({
    data: {
        logo: ['../../../images/logo1.png', '../../../images/logo2.png'],
        imgs: [
            "../../../images/weixin8.jpg",
            "../../../images/weixin3.jpg",
            "../../../images/weixin2.jpg",
        ],
        showDetail: true,
        section: [
            `
            &emsp;&emsp;微信公众平台,给个人、企业和组织提供业务服务与用户管理能力的全新服务平台。
            `,
            `
            &emsp;&emsp;有的人错过了2003年开淘宝的时机，又错过了微博营销，在2013年微信公众号火起来的时候大家似乎没有错过。于是一头扎进微信营销的浪潮中。腾讯公布的数据是现在有1500多万公众号，而且以每天10000个的速度在增加，一片欣欣向荣的画面。
            
            &emsp;&emsp;微信公众号已形成成熟的流量变现模式。经过数年发展，庞大的创作群 体加速了微信公众平台的快速发展，尤其是粉丝数量的激增促使公众号 从单纯内容输出向商业化、专业化转变：企业通过企业号、服务号发布 官方信息并直接与用户沟通，订阅号通过打赏、推广广告等方式进行流 量变现。微信公众号已形成广告推广、电商、内容付费、付费打赏等清 晰的商业模式，并围绕公众号产业链集聚了大量第三方运营企业。`,
            `
            
            小程序具有的优点： 

            &emsp;&emsp;&emsp;一丶 消息自动回复,关键词自动回复。
            &emsp;&emsp;&emsp;二丶 基于微信平台，拥有巨量的用户流量
            &emsp;&emsp;&emsp;三丶 深入渗透生活，获取信息的重要来源
            &emsp;&emsp;&emsp;四丶 能够和微信小程序进行关联
            &emsp;&emsp;&emsp;五丶 内容打赏等成熟的流量变现模式
            
            想了解更多微信公众号的信息请关注:
            `
        ]
    },
    showDetail: true,
    // function
    toggleDetail:utils.toggleDetail,
    makePhone:utils.makePhone,
});