var utils = require("../../../utils/utils");
Page({ 
    data: {
        logo: ['../../../images/logo1.png', '../../../images/logo2.png'],
        imgs: [
            "../../../images/transaction2.jpg",
            "../../../images/transaction.jpg",
            "../../../images/transaction3.jpg",
        ],
        showDetail: true,
        section: [
            `
            &emsp;&emsp;一、首先我们会根据您所提出的的需求给您做出分析，和提供相关的帮助，分析结束后将撰写开发草案。

            &emsp;&emsp;&emsp;您可以提供给我们您看中的网站风格，和希望设计的网站类型，并以链接、图片、短视频、口头描述、文字描述等方式告知我们。

            &emsp;&emsp;二、我们将为您分析设计中的时间开销，并根据您提出的具体的需求做出产品交付预期。为您解释开发流程，和您需要参与的环节

            `,
            `
            &emsp;&emsp;三、开发过程中的图片，商标，文章等涉及版权问题的资料需要客户您的提供，我们概不负责。

            &emsp;&emsp;四、内部分工，我们会根据你提出的需求做出开发方案并提供给接触这项开发工作的人员。

            &emsp;&emsp;五、页面的设计和页面的制作，过程中我们将不断进行调试，以图片和短视频的方式交于用户进行确认。定下开发的模板。

            &emsp;&emsp;六、开发模板确认后，不允许更改，此时进行后台逻辑结构设计和接口开发。


            `,
            `  
            &emsp;&emsp;七、网站测试和确认，平台的开通通知，期间需要您提供备案所需信息或者再我们的帮助下，您可以自己进行备案和服务器的购买。

            &emsp;&emsp;八、上传后台，经过详细的测试后，平台上线，进行产品交付。

            &emsp;&emsp;想了解更多交易流程信息请联系:


            `
        ]
    },
    showDetail: true,
    // function
   
    toggleDetail: utils.toggleDetail,
    makePhone: utils.makePhone,
});