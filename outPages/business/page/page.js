var utils = require("../../../utils/utils");
Page({
    data: {
        logo: ['../../../images/logo1.png', '../../../images/logo2.png'],
        imgs: [
            "../../../images/page3.jpg",
            "../../../images/page.jpg",
            "../../../images/ad0.jpg",
        ],
        showDetail: true,
        section: [
            `
            &emsp;&emsp;网站建设服务，为中小型企业、个人或组织搭建海报式宣传网站，满足客户的个性化需求，充分体现企业或组织的形象，提高知名度。

            &emsp;&emsp;于企业或组织来说，能够充分体现企业或组织形象，提高企业或组织知名度，于个人来说，能够提供更加个性化的开发服务。设计同时兼容手机、IPAD等触摸屏设备分辨率，达到最优访问效果，网站数据同步于各终端。
            `,
            `
            &emsp;我们的技术栈:
            &emsp;&emsp;前端:
            HTML5+CSS3+ES6+Bootstrap+jQuery+Vue/React
            &emsp;&emsp;后端:
            Python3+PHP+NodeJs+Java+C/C++
            &emsp;&emsp;后台操作系统:
            CentOs/Winserver 
            &emsp;&emsp;服务器:
            Node、Apache、Nginx、Tomcat、IIS
            &emsp;&emsp;数据库:
            Mysql、Oracle、MongoDB、SQL Server
            &emsp;&emsp;打包工具:
            webpack、Electron、Hbuilder
            &emsp;&emsp;开发工具:
            Sublime Text3、Eclipse、Pycharm、VsCode、WebStorm、Hbuilder、Android Studio
			设计出同时
            兼容手机、平板、电脑等分辨率电子设备。
            兼容android、IOS、Windows、Linux
            兼容Chrome,Edge,Opero,Firfox,Safair
            `,
            `
            
            具备功能:
            一、美观海报:美观大气的海报图、用于展示形象

            二、设计风格:现在最为流行的扁平式设计风格，简约主义。

            三、页面分级:网站结构分类，栏目管理，下级分类
            
            四、具体功能:
            &emsp;地图、定位、视频、音乐、上传、下载、脸部登陆、连接QQ\微信等
            
            &emsp;微信和支付宝支付、在线聊天、页内搜索、页内留言、新闻发布、调用外部视频、邮件和电话验证等等

            五、创意设计，根据用户提供的资源进行独具匠心的设计，注重用户体验

            &emsp;&emsp;想了解更多个人博客的信息请关注:
            `
        ]
    },
    showDetail: true,
    // function
    toggleDetail:utils.toggleDetail,
    makePhone:utils.makePhone,
});